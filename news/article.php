<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
$elementId = $_REQUEST['ELEMENT_ID'];
if(CModule::IncludeModule('iblock') && $arIBlockElement = CIBlockElement::GetByID($elementId)->Fetch())
?>
    <div class="l-content__big">
        <div class="l-content__section-news">
            <div class="l-news-page-title">
                <div class="b-title">
                    <a class="e-previous-page__title" href="/news">Новости</a>
                    <span class="e-sign-next__title">→</span>
                    Прицеп АВТОДОМ-МОТО на открытии мотосезона
                </div>
            </div>
            <div class="b-cms-area">
                <? echo $arIBlockElement['DETAIL_TEXT'] ?>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>