<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Каталог");
?>
<div class="l-content__big">
    <div class="l-horizontal-selector">
        <?$APPLICATION->IncludeComponent(
            "istok:list.products.types",
            ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => "8",
                "IBLOCK_ID_VANS" => "26",
                "IBLOCK_ID_TRAILERS" => "31",
                "IBLOCK_ID_SERVICES" => "33"
            ),
            false
        );?>
    </div>
    <div class="l-content__models">
        <?$APPLICATION->IncludeComponent(
            "istok:list.models",
            ".default",
            Array(
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "IBLOCK_TYPE" => "lists",
                "IBLOCK_ID" => "4"
            )
        );?>
    </div>
    <div class="l-content__section-catalog">
        <?$APPLICATION->IncludeComponent(
	"istok:list.catalog", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "26"
	),
	false
);?>
    </div>
</div>

<style>
    .b-text-block__header {
        margin-bottom: 15px;
    }
    .e-model__icon-man__simple-image {
        background-repeat: no-repeat;
        background-position: center;
        vertical-align: middle;
        position: relative;
        left: 10px;
    }
    .e-text__header {
        vertical-align: middle;
    }
</style>

<script src="/include/js/grayscale.js"></script>
<script src="/include/js/grayscale-init.js"></script>
<script>
    $(function() {
        $('body').on('click', '.e-horizontal-selector__item', function (e) {
            var modelId = '<? echo $selectedModelId = $_REQUEST['MODEL_ID']; ?>';
            var element = e.target;
            var elementId = $(element).attr('item-id');
            $('.e-horizontal-selector__item-selected').removeClass('e-horizontal-selector__item-selected').addClass("e-horizontal-selector__item");
            $(element).removeClass('e-horizontal-selector__item').addClass("e-horizontal-selector__item-selected");
            var data = {'PRODUCT_TYPE_IBLOCK_ID': elementId, 'MODEL_ID': modelId};
            $('.l-content__section-catalog').css({opacity: 1, visibility: "visible"}).animate({opacity: 0});
            $.ajax({
                type: "GET",
                async: false,
                url: "/api/getSelectorItems.php",
                cache: true,
                data: data,
                success: function (result) {
                    $('.l-content__models').empty();
                    $('.l-content__models').append(result);
                    setTimeout(function() {
                        $('.e-model__icon-generic').css({'filter': 'initial', '-webkit-filter': 'initial'});
                        grayscale($('.b-model-icon'));
                    }, 250)
                }
            });
            $.ajax({
                type: "GET",
                url: "/api/getCatalogItems.php",
                cache: true,
                data: data,
                success: function (result) {
                    $('.l-content__section-catalog').empty();
                    $('.l-content__section-catalog').append(result);
                    $('.l-content__section-catalog').css({opacity: 0, visibility: "visible"}).animate({opacity: 1});

                }
            });
        });
        $('body').on('click', '.b-model', function (e) {
            var element = $(e.currentTarget);
            var modelId = element.attr('item-id');
            if (modelId) {
                var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
                window.location.href = '/catalog/'+sectionId+'/'+modelId+'/';
            } else {
                var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
                var subSectionId = element.attr('section-id');;
                window.location.hash = 'section'+subSectionId;
                window.location.hash = ' ';
            }
        });
        var modelIcon = $('.b-model__selected .b-model-icon > div');
        var modelStyle = modelIcon.attr('style');
        $('.e-model__icon-man__simple-image').attr('style', modelStyle);
        grayscale($('.e-model__icon-man__simple-image'));
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
