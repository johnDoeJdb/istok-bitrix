window.onload = function() {
    setTimeout(function() {
        grayscale($('.b-model-icon'));
        $('.e-model__icon-generic').css({'filter': 'initial', '-webkit-filter': 'initial'});

        $('.b-model-icon').css({
            'filter': 'none',
            '-webkit-filter': 'initial',
        });
        $('body').on('mouseover', '.b-model', function() {
            grayscale.reset( $(this).find('.b-model-icon') );
            $(this).find('.b-model-icon').css('opacity', 1);
        });
        $('body').on('mouseout', '.b-model', function() {
            grayscale( $(this).find('.b-model-icon') );
            $(this).find('.b-model-icon').css('opacity', 0.7);
        });
    }, 0);
};

