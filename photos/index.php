<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Фотоальбом");

?><div class="l-content__big">
	<div class="l-content__section">
		<div class="l-news-page-title">
			<div class="b-title">
 <a class="e-previous-page__title" href="/media">Фото и видео</a> <span class="e-sign-next__title">→</span>
				«Исток» на выставке «Автотранс’12»
			</div>
		</div>
		<div class="b-media-grid">
			 <?$APPLICATION->IncludeComponent(
	"istok:photos.list",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "Media",
		"IBLOCK_ID" => "35"
	)
);?>
		</div>
	</div>
</div>

<?
$_REQUEST['BLOCK_JAVASCRIPT'] = <<<EOF
<div class="l-slider" style="display: none;">
	<div class="b-slider">
		<div class="e-previous-page__slider"></div>
		<img src="/include/images/photo-1.png" class="e-image__slider">

		<div class="e-next-page__slider"></div>
	</div>
</div>
<script>
	$(function () {
		var images = [];
		var imageNumber = 0;
		var allPhotos = $('.js-photo-slider');
		for (var i = 0; i < allPhotos.length; i++) {
			images.push(allPhotos.eq(i).attr('src'));
		}
		function getNextImage() {
			if (images[imageNumber + 1]) {
				imageNumber++;
			} else {
				imageNumber = 0;
			}

			return images[imageNumber];
		}

		function getPreviousImage() {
			if (images[imageNumber - 1]) {
				imageNumber--;
			} else {
				imageNumber = 0;
			}
			console.log(imageNumber);

			return images[imageNumber];
		}

		$('body').on('click', '.e-previous-page__slider', function (e) {
			var image = getPreviousImage();
			$('.e-image__slider').attr('src', image);
			e.stopPropagation();
		});
		$('body').on('click', '.e-next-page__slider', function (e) {
			var image = getNextImage();
			$('.b-slider > img').fadeOut(300, function() {
				$('.b-slider > img').attr('src', image);
			});
			$('.b-slider > img').fadeIn(100);
			e.stopPropagation();
		});
		$('body').on('click', '.js-photo-slider', function (e) {
			$('.l-slider').show(100);
			e.stopPropagation();
		});
		$('body').on('click', function (e) {
			$('.l-slider').hide(100);
		});
		var inOrder = false;
	});
</script>
EOF;
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

