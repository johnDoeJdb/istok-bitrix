<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Подробности");
    CModule::IncludeModule("iblock");
    CModule::IncludeModule("catalog");
    $elementId = $_REQUEST['ELEMENT_ID'];
    $element = CIBlockElement::GetByID($elementId)->Fetch();
    $productInfo = CCatalogSKU::GetProductInfo($elementId, $element['IBLOCK_ID']);
    $product = CIBlockElement::GetByID($productInfo['ID'])->Fetch();
    $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'] = $product['IBLOCK_ID'];
    $_REQUEST['PRODUCT_TYPE_IBLOCK_DIRECT'] = true;
    $elementName = $element['NAME'];
    $elementDetailText = $element['DETAIL_TEXT'];
    $basePriceItem = CPrice::GetBasePrice($elementId);
    $basePrice = intval($basePriceItem['PRICE']);
    $_REQUEST['BASE_PRICE'] = $basePrice;
    $iBlockId = $element['IBLOCK_ID'];
    $optionsList = CIBlockElement::GetProperty($element['IBLOCK_ID'], $elementId, "sort", "asc", Array("CODE"=>"OPTIONS"));
    $options = array();
    while($option = $optionsList->GetNext())
    {
        $section = CIBlockSection::GetByID($option['VALUE'])->Fetch();
        $elementList = CIBlockElement::GetList(
            Array('ID' => 'DESC'),
            Array("IBLOCK_ID"=> $section['IBLOCK_ID'], 'SECTION_ID' => $section['ID']),
            false,
            Array ()
        );
        $ids = array();
        while ($item = $elementList->getNext()) {
            $price = CPrice::GetBasePrice($element['ID']);
            $option = array(
                'ID' => $item['ID'],
                'SECTION_ID' => $section['ID'],
                'NAME' => $item['NAME'],
                'PRICE' => intval($price['PRICE']),
            );
            $ids[] = $element['ID'];
            $options[$section['ID']]['DESCRIPTION'] = $section['DESCRIPTION'];
            $options[$section['ID']]['ID'] = $section['ID'];
            $options[$section['ID']]['IBLOCK_ID'] = $section['IBLOCK_ID'];
            $options[$section['ID']]['ITEMS'][] = $option;
            $options[$section['ID']]['NAME'] = $section['NAME'];
        }
    }
    $parametersList = CIBlockElement::GetProperty($iBlockId, $elementId, array("sort" => "asc"), Array('CODE' => '%_PARAMETER'));
    $parameters = array();
    while($element = $parametersList->getNext()) {
        $parameters[] = array(
            'NAME' => $element['NAME'],
            'VALUE' => (is_array($element['VALUE']) ? $element['VALUE']['TEXT'] : $element['VALUE']),
        );
    }

    $picturesList = CIBlockElement::GetProperty($iBlockId, $elementId, array("sort" => "asc"), Array('CODE' => 'PICTURES'));
    $pictures = array();
    while($element = $picturesList->getNext()) {
        $pictures[] = CFile::GetPath($element['VALUE']);
    }

?>
<div class="l-content__big">
    <div class="l-content__big">
        <div class="l-horizontal-selector">
            <?
                $APPLICATION->IncludeComponent(
                    "istok:list.products.types",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "8",
                        "IBLOCK_ID_VANS" => "26",
                        "IBLOCK_ID_TRAILERS" => "31",
                        "IBLOCK_ID_SERVICES" => "33"
                    ),
                    false
                );
            ?>
        </div>
        <div class="l-content__models">
            <?
                $APPLICATION->IncludeComponent(
                    "istok:list.models",
                    ".default",
                    Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "IBLOCK_TYPE" => "lists",
                        "IBLOCK_ID" => "4"
                    )
                );
            ?>
        </div>
        <div class="l-content__section-view">
            <div class="l-view__content-left">
                <div class="b-carousel">
                    <? $counter = 0; foreach($pictures as $picture) { ?>
                        <? if ($counter == 0) { ?>
                            <img class="e-image__carousel" src="<? echo $picture ?>">
                            <div class="b-thumbnails__carousel">
                                <img class="e-thumbnail__carousel-selected" src="<? echo $picture ?>">
                        <? } else { ?>
                            <img class="e-thumbnail__carousel" src="<? echo $picture ?>">
                        <? } ?>
                    <? $counter++; } ?>
                    </div>
                </div>
                <div class="b-text-block__content-view">
                    <? echo $elementDetailText ?>
                </div>
            </div>
            <div class="l-view__content-right">
                <div class="b-text-block__header">
                        <span class="e-text__header">
                            «<? echo trim($elementName) ?>»
                        </span>
                    <span class="e-model__icon-man__simple-image"></span>
                </div>
                <div class="b-parameters__view">
                    <? $counter = 0; foreach($parameters as $parameter) { ?>
                        <div class="b-parameter <? echo $counter > 3 ? 'js-hidden-parameter' : '' ?>">
                            <div class="e-key__parameter"><? echo $parameter['NAME'] ?></div>
                            <div class="e-key__value"><? echo $parameter['VALUE'] ?></div>
                        </div>
                    <? $counter++;} ?>
                    <? if ($counter > 4) { ?>
                        <div class="b-parameter">
                            <div class="e-key__parameter"></div>
                            <div class="e-key__value">
                                <a href="#" class="e-toggle__parameters">Посмотреть все характеристики</a>
                            </div>
                        </div>
                    <? $counter++;} ?>
                </div>
                <div class="b-panel-selects">
                    <? foreach($options as $sectionId => $section) { ?>
                        <div section-id="<? echo $sectionId ?>" block-id="<? echo $section['IBLOCK_ID'] ?>" class="b-block__panel-selects">
                            <div class="js-popup-content" style="display: none">
                                <? echo $section['DESCRIPTION'] ?>
                            </div>
                            <div class="b-header__block">
                                <div class="e-title__header-block"><? echo $section['NAME'] ?></div>
                                <div class="e-help__header-block">Зачем она нужна?</div>
                            </div>
                            <div item-id="<? echo $option['ID'] ?>" section-id="<? echo $sectionId ?>" class="e-select-panel__block-selected js-option-block">
                                <span content="js-option-name">Нет</span>
                            </div>
                            <? foreach($section['ITEMS'] as $option) { ?>
                                <div item-id="<? echo $option['ID'] ?>" section-id="<? echo $sectionId ?>" class="e-select-panel__block js-option-block">
                                    <span class="js-option-name"><? echo $option['NAME'] ?></span>
                                    <span class="e-price__select-panel">+ <span class="js-option-price"><? echo number_format($option['PRICE'] , 0 , "." , " ") ?></span> <span class="b-price-sign">ь</span></span>
                                </div>
                            <? } ?>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    <?
    $APPLICATION->IncludeComponent(
	"istok:view.footer", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "product",
		"IBLOCK_ID" => "36",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	),
	false
);
    ?>
<div class="l-popup__view js-popup-manager" style="display: none;">
    <div class="b-popup__black-overlay">
        <div class="e-header__popup"></div>
        <div class="e-title__popup">Связаться с менеджером</div>
        <div class="b-content__popup">
            <img class="e-content__left-image" src="/include/images/popup-view-1.png">
            <div class="b-order__details">
                <div class="b-order-item">
                    <div class="e-order__item">Промтоварный фургон «Эконом»</div>
                    <div class="e-order__price"><? echo number_format($basePrice , 0 , "." , " ") ?> <span class="b-price-sign">ю</span></div>
                </div>
                <div class="b-order-parameters">
                    <? foreach($options as $sectionId => $section) { ?>
                        <div section-id="<? echo $section['ID'] ?>" class="b-order-parameter js-option-block-bind">
                            <div class="e-option__order-parameter"><? echo $section['NAME'] ?></div>
                            <div class="e-parameter__order-parameter js-option-name">Нет</div>
                            <div class="e-price__order-parameter"><span class="js-option-price">0</span> <span class="b-price-sign">ь</span></div>
                         </div>
                    <? } ?>
                    <div class="b-order-parameter__total">
                        <div class="e-option__order-parameter">Итого</div>
                        <div class="e-price__order-parameter-total"><span class="js-total-price"><? echo number_format($basePrice , 0 , "." , " ") ?></span> <span class="b-price-sign">ю</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-footer__popup">
            <div class="e-form-text__popup">
                Оставьте нам свой номер телефона или эл. почту, и мы свяжемся с вами
            </div>
            <form id="contact-manager-form">
                <div class="b-contact-block">
                    <input name="phone" type="text" class="e-input__contact-block js-input-phone" placeholder="Номер телефона">
                    <input name="email"  type="email" class="e-input__contact-block js-input-email" placeholder="Эл. почта">
                    <input type="button" disabled class="e-button__contact-block js-add-order" value="Свяжитесь со мной">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="l-popup__view-info js-popup-info" style="display: none;">
    <div class="b-popup__black-overlay">
        <div class="e-header__popup"></div>
        <div class="e-title__popup">Защита фургона сзади</div>
        <div class="b-content__popup">
            <div class="b-popup-block">
                <img class="e-image__popup-block" src="/include/images/popup-view-2.png">
                <div class="b-text__popup-block">
                    <div class="e-title__text">Противоподкатный брус</div>
                    Брус или иной объект, размещаемый в передней или задней части транспортных средств с больщим клиренсом низко к земле с целью предотвращения попадания более низких объектов под транспортное средство.
                </div>
            </div>
            <div class="b-popup-block">
                <img class="e-image__popup-block" src="/include/images/popup-view-2.png">
                <div class="b-text__popup-block">
                    <div class="e-title__text">Охранник с автоматом</div>
                    Охранник с автоматом надёжно защитит ваш грузовик сзади, сбоку, спереди и даже от вас самих. Для этой работы мы набираем самых крутых ребят со всех концов света, так что вы можете быть совершенно спокойны за сохранность вашего фургона.  </p>
                </div>
            </div>
        </div>
        <input type="button" class="e-button__contact-block-centered" value="Всё понятно">
    </div>
</div>
<script src="/include/js/grayscale.js"></script>
<script src="/include/js/grayscale-init.js"></script>
<script src="/include/js/jquery.validate.min.js"></script>
<script src="/include/js/jquery.maskedinput.min.js"></script>
<script>
    function formatPrice(price) {
        price = (price + ' ').trim();
        var reversed = price.split('').reverse().join('');
        var formatedRiversedPrice = '';
        var formatedPrice = '';
        var counter = 0;
        for (var i = 0; i < reversed.length; i++) {
            if (counter == 3) {
                counter = 0;
                formatedRiversedPrice += ' ';
            }
            formatedRiversedPrice += reversed[i];
            counter++;
        }
        formatedPrice = formatedRiversedPrice.split('').reverse().join('');

        return formatedPrice;
    }

    var basePrice = <? echo $basePrice ?>;
    var totalPrice = <? echo $basePrice ?>;
    var phoneMask = "+ 7 999 999 99 99";
    var optionsPrice = 0;
    $('.js-add-order').on('click', function() {
        var selectedItems = $( ".e-select-panel__block-selected" ).filter(
            function( index, element ) {
                var text = $(element).find('span').text();
                return( text !== 'Нет' );
            }
        );
        var selectedOptionsIds = [];
        $(selectedItems).each(function(index, element) {
            selectedOptionsIds.push($(element).attr('item-id'));
        });
        var phone = $('.js-input-phone').val();
        var email = $('.js-input-email').val();
        $('.js-input-phone').val('');
        $('.js-input-email').val('');
        var data = { 'ELEMENT_ID': <? echo $elementId ?>, 'OPTIONS_IDS': selectedOptionsIds, 'EMAIL': email, 'PHONE': phone};
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/api/addOrder.php",
            data: JSON.stringify(data),
            success: function (result) {
                if (result > 0) {
                    alert('Ваш заказ принят. Мы свяжемся с вами в ближайшее время');
                }
            }
        });
    });
    $('body').on('click', '.e-vertical-selector__item', function (e) {
        var element = e.target;
        $('.e-vertical-selector__item-selected').removeClass('e-vertical-selector__item-selected').addClass("e-vertical-selector__item");
        $(element).removeClass('e-vertical-selector__item').addClass("e-vertical-selector__item-selected");
    });
    $('body').on('click', '.e-thumbnail__carousel', function (e) {
        var element = e.target;
        var imagePath = $(element).attr('src');
        $('.e-image__carousel').fadeOut(200);
        $('.e-image__carousel').attr('src', imagePath);
        $('.e-image__carousel').fadeIn(200);
        $('.e-thumbnail__carousel-selected').removeClass('e-thumbnail__carousel-selected').addClass("e-thumbnail__carousel");
        $(element).removeClass('e-thumbnail__carousel').addClass("e-thumbnail__carousel-selected");
    });
    $('body').on('click', '.e-select-panel__block', function (e) {
        var element = $(e.currentTarget);
        var parent = $(element).parent();
        var sectionId = element.attr('section-id');
        var itemId = element.attr('item-id');
        parent.find('.e-select-panel__block-selected').removeClass('e-select-panel__block-selected js-option-block-selected').addClass("e-select-panel__block");
        $(element).removeClass('e-select-panel__block').addClass("e-select-panel__block-selected js-option-block-selected");
        $('body').trigger('updated_price');

        var optionName = element.find('.js-option-name').text();
        var optionPrice = element.find('.js-option-price').text();
        optionPrice = !!optionPrice.replace(" ", "") > 0 ? optionPrice  : 0 ;
        optionName = optionName ? optionName  : 'Нет' ;
        $('.js-option-block-bind[section-id="'+sectionId+'"] .js-option-name').text(optionName);
        $('.js-option-block-bind[section-id="'+sectionId+'"] .js-option-price').text(optionPrice);

        optionsPrice = 0;
        $('.js-option-block-selected .js-option-price').each(function(index, element) {
            var price = parseInt($(element).text().replace(" ", ""));
            optionsPrice += price;
        });
        totalPrice = basePrice + optionsPrice;
        totalPrice = formatPrice(totalPrice);
        $('.js-total-price').text(totalPrice);
    });
    $('body').on('click', '.e-toggle__parameters', function (e) {
        e.preventDefault();
        $('.js-hidden-parameter').show(100);
        $(this).hide();
    });
    $('.b-manager-button').on('click', function (e) {
        $('.js-popup-manager').fadeIn(150);
        e.stopPropagation();
    });
    $('.e-help__header-block').on('click', function (e) {
        $('.js-popup-info').empty();
        var parent = $(this).parents('.b-block__panel-selects');
        var blockId = parent.attr('block-id');
        var sectionId = parent.attr('section-id');
        var data = { 'PRODUCT_TYPE_IBLOCK_ID': blockId, 'SECTION_ID': sectionId};
        $.ajax({
            type: "GET",
            contentType: "text/html; charset=utf-8",
            url: "/api/getPopupContent.php",
            data: data,
            success: function (result) {
                $('.l-popup__view-info').hide().append(result).fadeIn(150);
            }
        });
        e.stopPropagation();
    });
    $('.e-header__popup').on('click', function (e) {
        $(e.currentTarget).parent().parent().hide();
    });
    $('.e-button__contact-block-centered').on('click', function (e) {
        $(e.currentTarget).parent().parent().hide();
    });
    $('.e-button__contact-block').on('click', function (e) {
        $('.l-popup__view').hide();
    });
    $('body').on('click', '*', function (e) {
        $('.js-popup-manager').hide();
        $('.js-popup-info').hide();
    });
    $('.b-popup__black-overlay').on('click', function (e) {
        e.stopPropagation();
    });
    $('.l-footer__fixed').css({});
    $('body').on('click', '.b-model', function (e) {
        var element = $(e.currentTarget);
        var modelId = element.attr('item-id');
        var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
        if (modelId) {
            var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
            window.location.href = '/catalog/'+sectionId+'/'+modelId+'/';
        } else {
            var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
            var subSectionId = element.attr('section-id');;
            window.location.href = '/catalog/'+sectionId+'/#section'+subSectionId;
        }
    });

    // Validation
    $.validator.addMethod("regex", function(value, element, regexpr) {
        return regexpr.test(value);
    }, "");
    $("#contact-manager-form").validate({
        onkeyup: false,
        onfocusout: false,
        errorElement: "div",
        errorPlacement: function(error, element) {
            error.appendTo("div#errors2");
        },
        rules: {
            "phone" : {
                required : true,
//                minlength: 5,
                regex: /^\+\s[0-9]+\s[0-9]{3}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}/
            },
            "email": {
                required : true,
                email : true,
                minlength: 2
            }
        },
        messages: {
            "phone": {
                required: "Телефон не должен быть пустым",
            },
            "email": {
                required: "Эл. почта не должна быть пустой",
                email: "Некорректный формат"
            }
        }
    });
    $('.js-input-phone').mask(phoneMask);
    $("#contact-manager-form input").on('change', function() {
        setInterval(function() {
            var orderButton = $('.js-add-order');
            if ($("#contact-manager-form").valid()) {
                orderButton.removeAttr('disabled');
            } else {
                orderButton.attr('disabled');
            }
        }, 100);
    })

</script>
