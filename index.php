<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Исток");           
?><div class="l-content">
	<div class="l-content__inner">
		<div class="l-horizontal-selector">
			 <?$APPLICATION->IncludeComponent(
	"istok:list.products.types", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "8",
		"IBLOCK_ID_VANS" => "26",
		"IBLOCK_ID_TRAILERS" => "31",
		"IBLOCK_ID_SERVICES" => "33"
	),
	false
);?>
		</div>
		<div class="l-content__models">
			 <?$APPLICATION->IncludeComponent(
	"istok:list.models",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "lists",
		"IBLOCK_ID" => "4"
	)
);?>
		</div>
		<div class="l-content__title">
			<div class="b-title">
				 Лучшие прицепы для ваших машин
			</div>
		</div>
		<div class="l-content__selector">
			 <?$APPLICATION->IncludeComponent(
	"istok:list.trailers", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "26"
	),
	false
);?>
		</div>
	</div>
</div>
<div class="l-two-columns">
	<div class="l-advantages">
		<?$APPLICATION->IncludeComponent(
	"istok:main.list.advantages", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "main_page",
		"IBLOCK_ID" => "25",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	),
	false
);?>
	</div>
	<div class="l-articles">
		 <?$APPLICATION->IncludeComponent(
	"istok:list.news",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "lists",
		"IBLOCK_ID" => "15"
	)
);?>
	</div>
</div>
<div class="b-separator">
</div>
<div class="l-awards">
	<div class="b-title__section-centered">
		 награды и сертификаты
	</div>
	 <?$APPLICATION->IncludeComponent(
	"istok:list.awards",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "lists",
		"IBLOCK_ID" => "10"
	)
);?>
</div>
<div class="b-separator">
</div>
<div class="l-media">
	 <?$APPLICATION->IncludeComponent(
	"istok:list.media.main", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "Media",
		"IBLOCK_ID_PHOTO" => "35",
		"IBLOCK_ID_VIDEO" => "12"
	),
	false
);?>
</div>
<script>

</script>
<?
$_REQUEST['BLOCK_JAVASCRIPT'] = <<<EOF
<div class="l-popup__preview" style="display: none;">
	<div class="b-popup__gray">
		<img class="b-preview__image" src="">
	</div>
</div>
<div class="l-video__preview" style="display: none;">
	<div class="b-video__container">
		<video id="example_video_1" class="video-js vjs-default-skin"
			   controls preload="auto" width="640" height="264"
			   poster=""
			   data-setup='{"example_option":true}'>
			<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser
				that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
		</video>
	</div>
</div>
<script src="/include/js/video.js"></script>
<script src="/include/js/enquire.min.js"></script>
<script src="/include/js/jquery.mousewheel.js"></script>
<script src="/include/js/jquery.jscrollpane.min.js"></script>
<script src="/include/js/grayscale.js"></script>
<script src="/include/js/grayscale-init.js"></script>
<script>
	String.prototype.capitalize = function() {
		return this.charAt(0).toUpperCase() + this.slice(1);
	};
	$(function() {
		$('body').on('click', '.e-horizontal-selector__item', function (e) {
			var element = e.target;
			var elementId = $(element).attr('item-id');
			$('.e-horizontal-selector__item-selected').removeClass('e-horizontal-selector__item-selected').addClass("e-horizontal-selector__item");
			$(element).removeClass('e-horizontal-selector__item').addClass("e-horizontal-selector__item-selected");
			$('.l-content__selector').css({opacity: 1, visibility: "visible"}).animate({opacity: 0});
			var data = {'PRODUCT_TYPE_IBLOCK_ID': elementId};
			$.ajax({
				type: "GET",
				async: false,
				cache: true,
				url: "/api/getSelectorItems.php",
				data: data,
				success: function (result) {
					$('.l-content__models').empty();
					$('.l-content__models').append(result);
					setTimeout(function() {
						$('.e-model__icon-generic').css({'filter': 'initial', '-webkit-filter': 'initial'});
						grayscale($('.b-model-icon'));
					}, 250)
				}
			});
			$.ajax({
				type: "GET",
				contentType: "application/json; charset=utf-8",
				url: "/api/getCatalogSections.php",
				data: data,
				cache: true,
				success: function (result) {
					$('.b-vertical-selector > li').remove();
					$('.b-selector-image > img').remove();
					$('.b-popup__selector > .e-popup__description').remove();
					$('.b-popup__selector > .e-popup__title').remove();
					var counter = 0;
					$(result).each(function(index, element) {
						counter++;
						var menuItem = $('<li>').attr('item-id', element.ID).addClass(counter == 1 ? 'e-vertical-selector__item-selected' : 'e-vertical-selector__item').text(element.NAME);
						$('.b-vertical-selector').append(menuItem);
						var img = $('<img>').attr('item-id', element.ID).attr('src', element.PICTURE);
						counter == 1 ? img.show() : img.hide();
						$('.b-selector-image').append(img);
						var description = $('<div>').attr('item-id', element.ID).addClass('e-popup__description').text(element.DESCRIPTION);
						var title = $('<div>').attr('item-id', element.ID).addClass('e-popup__title').text(element.POPUP_NAME);
						counter == 1 ? description.show() : description.hide();
						counter == 1 ? title.show() : title.hide();
						$('.b-popup__selector').prepend(description);
						$('.b-popup__selector').prepend(title);
					});
//					$('.b-popup__selector .e-popup__title').text($('.e-vertical-selector__item-selected').text());
					var catalogType = $('.e-horizontal-selector__item-selected').attr('item-id');
					var selectedId = $('.e-vertical-selector__item-selected').attr('item-id');
					$('.e-link__popup-selector').attr('href', '/catalog/'+catalogType+'/#section'+selectedId);
					$('.l-content__selector').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0});
				}
			});
		});
		$('body').on('click', '.e-vertical-selector__item', function (e) {
			var element = e.target;
			var selectedId = $(element).attr('item-id');
			var catalogType = $('.e-horizontal-selector__item-selected').attr('item-id');
			var itemName = $('.e-vertical-selector__item-selected').text().trim();
			$('.e-vertical-selector__item-selected').removeClass('e-vertical-selector__item-selected').addClass("e-vertical-selector__item");
			$(element).removeClass('e-vertical-selector__item').addClass("e-vertical-selector__item-selected");
			$('.e-popup__description').css({opacity: 1, display: "none"}).animate({opacity: 0});
			$('.b-selector-image img').css({opacity: 1, display: "none"}).animate({opacity: 0});
			$('.b-popup__selector .e-popup__title').css({opacity: 1, display: "none"}).animate({opacity: 0});
			$('.e-popup__description[item-id='+selectedId+']').css({opacity: 0, display: "block"}).animate({opacity: 1});
			$('.b-selector-image img[item-id='+selectedId+']').css({opacity: 0, display: "block"}).animate({opacity: 1});
			$('.b-popup__selector .e-popup__title[item-id='+selectedId+']').css({opacity: 0, display: "block"}).animate({opacity: 1});
			$('.e-link__popup-selector').attr('href', '/catalog/'+catalogType+'/#section'+selectedId);
		});
		$('body').on('click', '.e-award', function (e) {
			$('.l-popup__preview').css('display', 'block');
			var element = e.target;
			var imageSource = $(element).attr('src');
			$('.b-preview__image').attr('src', imageSource);
			e.preventDefault();
			e.stopPropagation();
		});
		var myPlayer = videojs('example_video_1');
		$('body').on('click', '.js-video-player', function (e) {
			$('.l-video__preview').show();
			var url = $(this).attr('data-link');
			myPlayer.src({ type: "video/mp4", src: url });
			myPlayer.currentTime(0);
			myPlayer.play();
			e.preventDefault();
			e.stopPropagation();
		});
		$('body').on('click', '.l-video__preview', function (e) {
			e.preventDefault();
			e.stopPropagation();
		});
		$('body').on('click', '.b-model', function (e) {
			var element = $(e.currentTarget);
			var modelId = element.attr('item-id');
			if (modelId) {
				var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
				window.location.href = '/catalog/'+sectionId+'/'+modelId+'/';
			} else {
				var sectionId = $('.e-horizontal-selector__item-selected').attr('item-id');
				var subSectionId = element.attr('section-id');;
				window.location.href = '/catalog/'+sectionId+'/#section'+subSectionId;
			}
		});
		$('body').on('click', function (e) {
			$('.l-video__preview').hide();
			$('.l-popup__preview').hide();
			myPlayer.pause();
			$('#example_video_1 source').attr('src', '');
		});
		$('body').on('click', '.e-thumbnail__carousel', function (e) {
			var element = e.target;
			$('.e-thumbnail__carousel-selected').removeClass('e-thumbnail__carousel-selected').addClass("e-thumbnail__carousel");
			$(element).removeClass('e-thumbnail__carousel').addClass("e-thumbnail__carousel-selected");
		});

		$('.js-scroll-pane').jScrollPane({
			horizontalDragMaxWidth  : 400,
			horizontalGutter: 20,
			autoReinitialise: true
		});
	});
</script>
EOF;
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
