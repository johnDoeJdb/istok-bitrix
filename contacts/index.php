<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<div class="l-content__big">
	<div style="clear: both;overflow: hidden;">
	</div>
	<div class="l-content__section-contacts">
		<div class="l-news-page-title">
			<div class="b-title">
				 Контакты
			</div>
		</div>
		<div class="l-content__section-left">
			<div class="l-contacts-text-block">
				<div class="b-title__secondary">
					 адрес и телефон
				</div>
				 <?$APPLICATION->IncludeComponent(
	"istok:contacts.full_address",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "contacts",
		"IBLOCK_ID" => "16",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);?> <?$APPLICATION->IncludeComponent(
	"istok:contacts.phone",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "contacts",
		"IBLOCK_ID" => "20",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);?>
			</div>
			 <?$APPLICATION->IncludeComponent(
	"istok:contacs.work.time",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "contacts",
		"IBLOCK_ID" => "17",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);?>
</div>
		<div class="l-content__feedback-form">
			<div class="b-title__secondary">обратная связь</div>
			<?$APPLICATION->IncludeComponent(
	"bitrix:form", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"START_PAGE" => "new",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"SUCCESS_URL" => "",
		"WEB_FORM_ID" => "1",
		"RESULT_ID" => $_REQUEST[RESULT_ID],
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_STATUS" => "Y",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "Y",
		"NOT_SHOW_FILTER" => array(
			0 => "",
			1 => "",
		),
		"NOT_SHOW_TABLE" => array(
			0 => "",
			1 => "",
		),
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		)
	),
	false
);?>
		</div>
<div class="l-map">
	<?$APPLICATION->IncludeComponent(
		"istok:contacts.map",
		".default",
		Array(
			"COMPONENT_TEMPLATE" => ".default",
			"IBLOCK_TYPE" => "contacts",
			"IBLOCK_ID" => "19",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600"
		)
	);?>
</div>
</div>
</div>
 &nbsp;<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>