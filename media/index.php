<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Медиа");
?><div class="l-content__big">
	<div style="clear: both;overflow: hidden;">
	</div>
	<div class="l-content__section">
		<div class="l-news-page-title">
			<div class="b-title">
				 Фото и видео
			</div>
		</div>
		<div class="b-media-grid">
			<div class="l-media-grid__left">
				<div class="b-title__secondary">
					 фотоальбомы
				</div>
				 <?$APPLICATION->IncludeComponent(
	"istok:media.photoalbum.list",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "Media",
		"IBLOCK_ID" => "35"
	)
);?>
			</div>
			<div class="l-media-grid__right">
				<div class="b-title__secondary">
					 ВИДЕОМАТЕРИАЛЫ
				</div>
				 <?$APPLICATION->IncludeComponent(
	"istok:media.video.list",
	"",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"IBLOCK_TYPE" => "Media",
		"IBLOCK_ID" => "12"
	)
);?>
			</div>
		</div>
	</div>
</div>
<div class="l-video__preview" style="display: none;">
	<div class="b-video__container">
		<video id="example_video_1" class="video-js vjs-default-skin"
			   controls preload="auto" width="640" height="264"
			   poster=""
			   data-setup='{"example_option":true}'>
			<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser
				that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
		</video>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

<script src="/include/js/video.js"></script>
<script>
	var myPlayer = videojs('example_video_1');
	$('body').on('click', '.js-video-player', function (e) {
		$('.l-video__preview').show();
		var url = $(this).attr('data-link');
		myPlayer.src({ type: "video/mp4", src: url });
		myPlayer.currentTime(0);
		myPlayer.play();
		e.preventDefault();
		e.stopPropagation();
	});
	$('body').on('click', '.l-video__preview', function (e) {
		e.preventDefault();
		e.stopPropagation();
	});
	$('body').on('click', function (e) {
		$('.l-video__preview').hide();
		$('.l-popup__preview').hide();
		myPlayer.pause();
		$('#example_video_1 source').attr('src', '');
	});
</script>
