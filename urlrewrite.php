<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/catalog/([0-9]+)/?([0-9]+)?/?#",
		"RULE" => "PRODUCT_TYPE_IBLOCK_ID=\$1&MODEL_ID=\$2",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#([a-zA-Z\\-]+)\\/?#",
		"RULE" => "\$1.php",
	),
	array(
		"CONDITION" => "#^/news/([0-9]+)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "istok:list.news",
		"PATH" => "/news/article.php",
	),
	array(
		"CONDITION" => "#^/view/([0-9]+)#",
		"RULE" => "ELEMENT_ID=\$1&PRODUCT_TYPE_IBLOCK_DIRECT=true",
		"ID" => "bitrix:catalog",
		"PATH" => "/view/index.php",
	),
	array(
		"CONDITION" => "#^/photos/([0-9]+)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "bitrix:catalog",
		"PATH" => "/photos/index.php",
	),
);

?>