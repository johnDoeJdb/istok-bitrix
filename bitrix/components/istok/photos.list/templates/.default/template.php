<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?
	CModule::IncludeModule("iblock");
	$elementId = $_REQUEST['ELEMENT_ID'];
	$photoList = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $elementId, "sort", "asc", Array("CODE"=>"photoalbums"));
	$photos = array();
	while($photoField = $photoList->GetNext())
	{
		$photos[] = CFile::GetPath($photoField['VALUE']);
	}
?>

<div class="b-photos__photos-page">
	<?foreach($photos as $photo){?>
		<a href="#" class="b-photo__photo-page">
			<img class="e-photo__image js-photo-slider" src="<? echo $photo ?>">
		</a>
	<?}?>
</div>