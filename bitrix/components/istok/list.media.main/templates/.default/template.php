<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?
	CModule::IncludeModule("iblock");
	$listPictures = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> array($arParams['IBLOCK_ID_PHOTO'])));
	$listVideo = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> array($arParams['IBLOCK_ID_VIDEO'])));
    $mediaList = array();
	$photoCounter = 0;
	$videoCounter = 0;
	const VIDEO_TYPE = 0;
	const PHOTO_TYPE = 1;
	while($element = $listPictures->GetNextElement())
	{
		if ($photoCounter < 3) {
			$fields = $element->GetFields();
			$name = $fields['NAME'];
			$previewImage = CFile::GetPath($fields['PREVIEW_PICTURE']);
			$media = new \stdClass();
			$media->id = $fields['ID'];
			$media->type = PHOTO_TYPE;
			$media->preview = $previewImage;
			$media->name = $fields['NAME'];
			$media->url = $image;
			$mediaList[] = $media;
			$photoCounter++;
		} else {
			break;
		}
	}
	while($element = $listVideo->GetNextElement())
	{
		if ($videoCounter < 2) {
			$fields = $element->GetFields();
			$name = $fields['NAME'];
			$res = CIBlockElement::GetProperty($arParams['IBLOCK_ID_VIDEO'], $fields['ID'], "sort", "asc", array("CODE" => "VIDEO"));
			$videoField = $res->Fetch();
			$videoId = $videoField['VALUE'];
			$media = new \stdClass();
			$media->id = $fields['ID'];
			$media->type = VIDEO_TYPE;
			$media->preview = CFile::GetPath($fields['PREVIEW_PICTURE']);
			$media->name = $fields['NAME'];
			$media->url = CFile::GetPath($videoId);
			$mediaList[] = $media;
			$videoCounter++;
		} else {
			break;
		}
	}
?>

<div class="b-media-container">
	<div class="b-title__section-centered">
		<a href="/media/" class="e-title__articles-primary">фото и видео</a>
	</div>
	<? foreach($mediaList as $media) { ?>
		<div class="b-media">
			<a data-link="<? echo $media->url ?>" class="b-media__link <? echo $media->type == VIDEO_TYPE ? 'js-video-player' : '' ?>" href="<? echo $media->type == VIDEO_TYPE ? 'javascript:void(0);' : '/photos/'.$media->id ?>">
				<div class="e-media__thumbnail">
					<? if ($media->type == VIDEO_TYPE) { ?>
						<div class="b-play__thumbnail">
							<div class="e-triangle__play"></div>
						</div>
					<? } ?>
					<img src="<? echo $media->preview ?>">
				</div>
				<div class="e-media__description">
					<? echo $media->name ?>
				</div>
			</a>
		</div>
	<? } ?>
</div>
