<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock"))
	return;

$arIBlockType = array();
$rsIBlockType = CIBlockType::GetList(array("sort"=>"asc"), array("ACTIVE"=>"Y"));
while ($arr=$rsIBlockType->Fetch())
{
	if($ar=CIBlockType::GetByIDLang($arr["ID"], LANGUAGE_ID))
	{
		$arIBlockType[$arr["ID"]] = "[".$arr["ID"]."] ".$ar["~NAME"];
	}
}

$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y"),
		"IBLOCK_ID_PHOTO" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_IBLOCK").' фото',
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock),
		"IBLOCK_ID_VIDEO" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_IBLOCK").' видео',
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600)
	),
);
?>

<div class="b-media-container">
	<div class="b-title__section-centered">
		<a href="./media.html" class="e-title__articles-primary">фото и видео </a>
	</div>
	<div class="b-media">
		<a class="b-media__link" href="./photos.html">
			<div class="e-media__thumbnail">
				<img src="./images/thumbnail-4.png">
			</div>
			<div class="e-media__description">
				«Исток» на выставке «Автотранс’12»
			</div>
		</a>
	</div>
	<div class="b-media">
		<a class="b-media__link js-video-player" href="#">
			<div class="e-media__thumbnail">
				<img src="./images/thumbnail-2.png">
			</div>
			<div class="e-media__description">
				Прицеп Исток на Чемпионате мира по спидвею
			</div>
		</a>
	</div>
	<div class="b-media">
		<a class="b-media__link js-video-player" href="#">
			<div class="e-media__thumbnail">
				<img src="./images/thumbnail-2.png">
			</div>
			<div class="e-media__description">
				Прицеп-рефрижератор ИСТОК 3791Т
			</div>
		</a>
	</div>
	<div class="b-media">
		<a class="b-media__link" href="./photos.html">
			<div class="e-media__thumbnail">
				<img src="./images/thumbnail-4.png">
			</div>
			<div class="e-media__description">
				Комтранс 2011
			</div>
		</a>
	</div>
	<div class="b-media">
		<a class="b-media__link" href="./photos.html">
			<div class="e-media__thumbnail">
				<img src="./images/thumbnail-4.png">
			</div>
			<div class="e-media__description">
				Прицеп ИСТОК 3791М2
			</div>
		</a>
	</div>
</div>
