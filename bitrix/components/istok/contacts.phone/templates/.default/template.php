<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ()
	);
	$element = $list->GetNextElement();
	$fields = $element->GetFields();
	$db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $fields['ID'], "sort", "asc", Array("CODE"=> 'PHONE'));
	$item = $db_props->GetNext();
?>
<div class="e-phone__contacts">
	<? echo $item['VALUE'] ?>
</div>