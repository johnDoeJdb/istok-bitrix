<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?

	$list = CIBlockElement::GetList(
		Array('ID' => 'ASC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ()
	);
	$advantages = array();
	while($element = $list->GetNextElement()) {
		$fields = $element->GetFields();
		$advantage = new \stdClass();
		$advantage->picture = CFile::GetPath($fields['PREVIEW_PICTURE']);
		$advantage->text= $fields['PREVIEW_TEXT'];
		$advantages[] = $advantage;
	}

	?>
		<div class="b-advantages">
			<div class="b-advantages__header">
				ПРЕИМУЩЕСТВА «Истока»
			</div>
			<? foreach ($advantages as $advantage) { ?>
				<div class="b-advantage">
					<div class="b-advantage__icon">
						<img src="<? echo $advantage->picture ?>">
					</div>
					<div class="b-advantage__description">
						<? echo $advantage->text ?>
					</div>
				</div>
			<? } ?>
		</div>