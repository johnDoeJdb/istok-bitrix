<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");
	$productTypeIBlockId = $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'] ? $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'] : $arParams['IBLOCK_ID'];
	$selectedModelId = $_REQUEST['MODEL_ID'];
	$mxResult = CCatalogSKU::GetInfoByProductIBlock($productTypeIBlockId);
	$arFilter = array('IBLOCK_ID' => $productTypeIBlockId, 'DEPTH_LEVEL' => 1);
	$sectionsList = CIBlockSection::GetList(array('ID' => 'asc'), $arFilter);
	$result = array();
	while ($sectionInfo = $sectionsList->GetNext())
	{
		$result[$sectionInfo['ID']]['NAME'] = $sectionInfo['NAME'];
		$result[$sectionInfo['ID']]['DESCRIPTION'] = $sectionInfo['DESCRIPTION'];
		$result[$sectionInfo['ID']]['ITEMS'] = array();
	}
    $sortPrice = function ($a, $b) {
        if ($a['PRICE'] == $b['PRICE']) {
            return 0;
        }
        return ($a['PRICE'] < $b['PRICE']) ? -1 : 1;
    };
	foreach ($result as $id => $section)
	{
		$elementList = CIBlockElement::GetList(
			Array('ID' => 'DESC'),
			Array("IBLOCK_ID"=> $productTypeIBlockId, 'SECTION_ID' => $id),
			false,
			Array ()
		);
		$ids = array();
		while ($element = $elementList->getNext()) {
			$ids[] = $element['ID'];
		}
		$offersResult = CCatalogSKU::getOffersList(
			$productID = $ids, // массив ID товаров
			$productTypeIBlockId, // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
			$skuFilter = array('IBLOCK_ID' => $mxResult['IBLOCK_ID']), // дополнительный фильтр предложений. по умолчанию пуст.
			$fields = array('NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'SECTION_ID', 'PRICE'),  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
			$propertyFilter = array()
		);
		$offersResultValues = array_values($offersResult);
		foreach($offersResultValues[0] as $offerId => $offer) {
			if ($selectedModelId) {
				$models = CIBlockElement::GetProperty($mxResult['IBLOCK_ID'], $offerId, "sort", "asc", Array("CODE"=>"MODEL"));
				$availableModels = array();
				while ($model = $models->getNext()) {
					if ($model['VALUE']) {
						$availableModels[] = $model['VALUE'];
					}
				}
				if ($availableModels && !in_array($selectedModelId, $availableModels)) {
					continue;
				}
			}
			$arPrice = CPrice::GetBasePrice($offerId);
			$price = number_format(intval($arPrice['PRICE']), 0, ',', ' ');
			$result[$id]['ITEMS'][] = array(
				'ID' => $offerId,
				'NAME' => $offer['NAME'],
				'PRICE' => $price,
				'DESCRIPTION' => $offer['PREVIEW_TEXT'],
				'PICTURE' => CFile::GetPath($offer['PREVIEW_PICTURE']),
			);
			usort($result[$id]['ITEMS'], $sortPrice);
		}
	}
?>

<? foreach ($result as $sectionId => $section) { ?>
	<div id="section<? echo $sectionId ?>" class="b-text-block">
		<div class="b-text-block__header">
            <span class="e-text__header">
                <? echo $section['NAME']?>
            </span>
			<span class="e-model__icon-man__simple-image"></span>
		</div>
		<div class="b-text-block__content">
			<? echo $section['DESCRIPTION'] ?>
		</div>
		<?
		$numberElements = count($section['ITEMS']);
		$counter = 0;
		$html = '';
		foreach ($section['ITEMS'] as $item) {
			$html .= <<<EOF
				<div class="l-column__grid-2-columns">
					<div class="b-model__catalog">
						<a href="/view/{$item['ID']}">
							<img class="e-image__model-catalog" src="{$item['PICTURE']}">
						</a>
						<div class="l-left-block__model-catalog">
							<div class="b-price__model-catalog">
								<a href="/view/{$item['ID']}" class="e-type__price">{$item['NAME']}</a>
								<div class="e-price__price">от {$item['PRICE']} <span class="e-rouble__price">ь</span></div>
							</div>
						</div>
						<div class="l-right-block__model-catalog">
							<a class="e-link__model-catalog" href="/view/{$item['ID']}">
								<input type="button" class="e-calc-button__price" value="Рассчитать стоимость">
							</a>
						</div>
						<div class="b-text-block__content">
							{$item['DESCRIPTION']}
						</div>
					</div>
				</div>
EOF;
			$counter++;
			if ($counter % 2 === 0 || $numberElements === $counter) {
				$html = '<div class="l-grid__2-column">'.$html.'</div>';
				echo $html;
				$html = '';
			} if ($numberElements === $counter) {
				$html = '<div class="l-grid__2-column">'.$html.'</div>';
				echo $html;
				$counter = 0;
				$html = '';
			}
		} ?>
	</div>
<? } ?>