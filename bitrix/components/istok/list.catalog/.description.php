<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 20,
	"PATH" => array(
		"ID" => "list",
		"CHILD" => array(
			"ID" => "trailers",
			"NAME" => GetMessage("T_IBLOCK_LIST_TRAILERS"),
			"SORT" => 20,
			"CHILD" => array(
				"ID" => "list_trailers",
			),
		),
	),
);

?>