<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ("nTopCount" => 8)
	);

	$videos = array();
	while($album = $list->GetNext())
	{
		$videoField = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $album['ID'], "sort", "asc", array("CODE" => "VIDEO"))->Fetch();
		$videoId = $videoField['VALUE'];
		$picture = CFile::GetPath($album['PREVIEW_PICTURE']);
		$video = CFile::GetPath($videoId);
		$videos[] = array(
			'ID' => $album['ID'],
			'NAME' => $album['NAME'],
			'PICTURE' => $picture,
			'VIDEO' => $video,
		);
	}
?>

<div class="b-videos">
	<?foreach($videos as $video){?>
		<a data-link="<? echo $video['VIDEO'] ?>" href="javascript:void(0);" class="b-video js-video-player">
			<div class="b-image-container__video">
				<div class="b-play__thumbnail-media">
					<div class="e-triangle__play-media"></div>
				</div>
				<img class="e-video__image" src="<? echo $video['PICTURE'] ?>">
			</div>
			<div class="e-video__title">
				<? echo $video['NAME'] ?>
			</div>
		</a>
	<?}?>
</div>