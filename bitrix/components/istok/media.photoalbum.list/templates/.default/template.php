<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ("nTopCount" => 8)
	);
	$albums = array();
	while($album = $list->GetNext())
	{
		$picture = CFile::GetPath($album['PREVIEW_PICTURE']);
		$albums[] = array(
			'ID' => $album['ID'],
			'NAME' => $album['NAME'],
			'PICTURE' => $picture,
		);
	}
?>

<div class="b-photos">
	<?foreach($albums as $album){?>
		<a href="/photos/<? echo $album['ID'] ?>" class="b-photo"> <img src="<? echo $album['PICTURE'] ?>" class="e-photo__image">
			<div class="e-photo__title">
				<? echo $album['NAME'] ?>
			</div>
		</a>
	<?}?>
</div>