<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
$productTypeIBlockId = $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'] ? $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'] : $arParams['IBLOCK_ID_VANS'];
$_REQUEST['PRODUCT_TYPE_IBLOCK_ID'] = $productTypeIBlockId;
?>
<ul class="b-horizontal-selector">
	<?
	$types = array();
	$elements = CIBlock::GetList(
		array(),
		array('ID' => array($arParams['IBLOCK_ID_VANS'], $arParams['IBLOCK_ID_TRAILERS'], $arParams['IBLOCK_ID_SERVICES']))
	);
	while ($type = $elements->getNext()) {
		$onclick = $_REQUEST['PRODUCT_TYPE_IBLOCK_DIRECT'] ? "location.href='/catalog/".$type['ID']."'" : '';
		$border = $_REQUEST['PRODUCT_TYPE_IBLOCK_DIRECT'] ? "border-bottom: #4d9acb solid 1px;" : '';
	?>
		<style>
			.e-horizontal-selector__item {
				<? echo $border ?>
			}
		</style>
		<li onclick="<? echo $onclick ?>"
			item-id="<? echo $type['ID'] ?>"
			class="e-horizontal-selector__item<? echo $productTypeIBlockId == $type['ID'] ? '-selected' : ''?>">
			<? echo $type['NAME'] ?>
		</li>
	<?}?>
</ul>