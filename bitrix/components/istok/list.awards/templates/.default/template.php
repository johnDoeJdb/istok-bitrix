<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="b-awards js-scroll-pane">
	<?
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']));
	while($element = $list->GetNextElement())
	{
		$blockId = $arParams['IBLOCK_ID'];
		$blockTypeId = $arParams['IBLOCK_ID'];
		$fields = $element->GetFields();
		$elementId = $fields['ID'];
		$elementTypeId = $fields['IBLOCK_ID'];
		$picture = $fields['PICTURE'];

		if ($blockTypeId != $elementTypeId) {continue;}
		$db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'PICTURE'));
		$picture = $db_props->GetNext();
		$image = CFile::GetPath($picture["VALUE"]);
		?>
			<img src="<? echo $image ?>" class="e-award">
		<?
	}
?>
</div>
