<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?
	CModule::IncludeModule("iblock");



	$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'DEPTH_LEVEL' => 1);
	$rsSect = CIBlockSection::GetList(array('ID' => 'asc'),$arFilter);
	$sections = array();
	while ($arSect = $rsSect->GetNext())
	{
		$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'],'ID'=>$arSect['ID'], 'GLOBAL_ACTIVE'=>'Y');
		$db_list = CIBlockSection::GetList(Array(), $arFilter, false, Array("UF_POPUP_TITLE"))->Fetch();

		$path = CFile::GetPath($arSect['PICTURE']);
		$sections[] = array(
			'ID' => $arSect['ID'],
			'POPUP_NAME' => $db_list['UF_POPUP_TITLE'],
			'NAME' => $arSect['NAME'],
			'DESCRIPTION' => $arSect['DESCRIPTION'],
			'PICTURE' => $path,
		);
	}
?>

<div class="b-selector">
	<ul class="b-vertical-selector">
		<?$counter = 0;foreach($sections as $id => $trailer){$counter++;?>
			<li item-id="<? echo $trailer['ID'] ?>" class="e-vertical-selector__item<? echo $counter === 1 ? '-selected' : '' ?>"><? echo $trailer['NAME'] ?></li>
		<?}?>
	</ul>
	<div class="b-selector-image">
		<?$counter = 0;foreach($sections as $id => $trailer){$counter++;?>
			<img style="display: <? echo $counter === 1 ? 'block' : 'none' ?>;" item-id="<? echo $trailer['ID'] ?>" src="<? echo $trailer['PICTURE'] ?>">
		<?}?>
	</div>
	<div class="b-popup__selector">
		<?$counter = 0;foreach($sections as $id => $trailer){$counter++;?>
			<div item-id="<? echo $trailer['ID'] ?>" class="e-popup__title" style="display: <? echo $counter === 1 ? 'block' : 'none' ?>;"><? echo $trailer['POPUP_NAME']; ?></div>
			<div item-id="<? echo $trailer['ID'] ?>" style="display: <? echo $counter === 1 ? 'block' : 'none' ?>;" class="e-popup__description"><? echo $trailer['DESCRIPTION'] ?></div>
		<?}?>
		<a class="e-link__popup-selector" href="/catalog/<? echo $arParams['IBLOCK_ID'] ?>#section<? echo $sections[0]['ID'] ?>">
			<input type="button" class="e-popup__footer-button" value="Посмотреть подробнее">
		</a>
	</div>
</div>

