<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ()
	);
	$element = $list->GetNextElement();
	$fields = $element->GetFields();
	$db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $fields['ID'], "sort", "asc", Array("CODE"=> 'ADDRESS'));
	$fullAddress = $db_props->GetNext();
?>
	<div class="e-text__contacts">
		<? echo $fullAddress['VALUE']['TEXT'] ?>
	</div>