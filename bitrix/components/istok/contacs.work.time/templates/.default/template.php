<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ()
	);
	$element = $list->GetNextElement();
	$fields = $element->GetFields();
	$db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $fields['ID'], "sort", "asc", Array("CODE"=> 'WORK_TIME'));
	$workTime = $db_props->GetNext();
	$db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $fields['ID'], "sort", "asc", Array("CODE"=> 'CHECK_TIME'));
	$checkTime = $db_props->GetNext();

?>
	<div class="l-contacts-text-block">
		<div class="b-title__secondary">время работы</div>
		<div class="e-text__contacts-regular">
			<? echo $workTime['VALUE']['TEXT'] ?>
		</div>
		<div class="e-text__work-time">
			<? echo $checkTime['VALUE']['TEXT'] ?>
		</div>
	</div>