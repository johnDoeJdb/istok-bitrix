<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 20,
	"PATH" => array(
		"ID" => "list",
		"CHILD" => array(
			"ID" => "contacts",
			"NAME" => GetMessage("T_IBLOCK_TEXT_CONTACTS"),
			"SORT" => 20,
			"CHILD" => array(
				"ID" => "text_contacts",
			),
		),
	),
);

?>