<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("iblock");
$list = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']));
while($element = $list->GetNextElement())
{
	$blockId = $arParams['IBLOCK_ID'];
	$fields = $element->GetFields();
	$elementId = $fields['ID'];
	$elementTypeId = $fields['IBLOCK_ID'];
	$picture = $fields['PICTURE'];
	if ($blockId != $elementTypeId) {continue;}
	$db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'PICTURE'));
	$picture = $db_props->GetNext();
	$image = CFile::GetPath($picture["VALUE"]);
	$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) == '/';
}

?>

<? if ($path) { ?>
	<div style="background-image: url('<? echo $image ?>') no-repeat" class="b-logo">
		<span class="e-slogan__logo">Работаем с 1992 года</span>
	</div>
<? } else { ?>
	<a class="b-logo__link" href="/">
		<div class="b-logo">
			<span class="e-slogan__logo">Работаем с 1992 года</span>
		</div>
	</a>
<? } ?>