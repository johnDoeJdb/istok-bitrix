<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ()
	);
	$element = $list->GetNextElement();
	$fields = $element->GetFields();
	$db_props = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $fields['ID'], "sort", "asc", Array("CODE"=> 'ADDRESS'));
	$item = $db_props->GetNext();
?>

<div id="map" style="width: 996px; height: 380px;position: relative;z-index: 20;"></div>
<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script>
	$.ajax({
		url: "https://geocode-maps.yandex.ru/1.x/?format=json&geocode=<? echo $item['VALUE'] ?>",
		success: function( data ) {
			ymaps.ready(init);
			function init() {
				var coordinates = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(" ");
				var map = new ymaps.Map("map", {
					center: [coordinates[1], coordinates[0]],
					zoom: 16
				});
				var placemark = new ymaps.Placemark([coordinates[1], coordinates[0]], {});

				map.geoObjects.add(placemark);
				myMap.behaviors.disable('scrollZoom');

				$("ymaps[class*='ymaps']").eq(2).each(function (i, el) {
					$(el).css('-webkit-box-shadow', 'inset 10px 10px 156px -23px rgba(0,0,0,0.3)');
					$(el).css('-moz-box-shadow', 'inset 10px 10px 156px -23px rgba(0,0,0,0.3)');
					$(el).css('box-shadow', 'inset 10px 10px 156px -23px rgba(0,0,0,0.3)');
				});
			}
		}
	});
</script>