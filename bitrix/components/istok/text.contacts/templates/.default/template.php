<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']));
	while($element = $list->GetNextElement())
	{
		$blockId = $arParams['IBLOCK_ID'];
		$blockTypeId = $arParams['IBLOCK_ID'];
		$fields = $element->GetFields();
		$elementId = $fields['ID'];
		$elementTypeId = $fields['IBLOCK_ID'];
		$picture = $fields['PICTURE'];

		if ($blockTypeId != $elementTypeId) {continue;}
			$db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'PHONE'));
			$phone = $db_props->GetNext();
			$db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'EMAIL'));
			$email = $db_props->GetNext();
	        $db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'CITY'));
	        $city = $db_props->GetNext();
	        $db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'ADDRESS'));
	        $address = $db_props->GetNext();
	}
?>
<style>
	.bx-context-toolbar-empty-area {
		background: transparent !important;
	}
</style>
	<span style="color: transparent; height: 0px; width: 0; float: left;">прицепы</span>
	<div class="b-two-lines">
		<div class="e-line__phone"><? echo $phone['VALUE']?></div>
		<div class="b-line">
			<a class="e-line__link" href="mailto:<? echo $email['VALUE']?>"><? echo $email['VALUE']?></a>
		</div>
	</div>
	<div class="b-two-lines_map">
		<a class="e-line__link-map-icon" href="/contacts#map">
			<div class="e-two-lines__map-icon"></div>
		</a>

		<div class="b-two-lines__inner">
			<div class="e-line__address"><? echo $city['VALUE']?></div>
			<a class="e-line__link-map" href="/contacts#map"><? echo $address['VALUE']?></a>
		</div>
	</div>