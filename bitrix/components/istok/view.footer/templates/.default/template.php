<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule("iblock");
	$arFilter = Array("IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']));
	$element = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), Array())->Fetch();
	$elementId = $element['ID'];

	$properties['PHONE'] = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $elementId, "sort", "asc", Array("CODE"=>"PHONE"))->Fetch();
	$properties['TEXT_ABOVE_PHONE'] = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $elementId, "sort", "asc", Array("CODE"=>"TEXT_ABOVE_PHONE"))->Fetch();
	$properties['TEXT_ABOVE_PRICE'] = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $elementId, "sort", "asc", Array("CODE"=>"TEXT_ABOVE_PRICE"))->Fetch();
	$properties['TEXT_BUTTON'] = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $elementId, "sort", "asc", Array("CODE"=>"TEXT_BUTTON"))->Fetch();
?>

	<div class="l-footer__fixed">
		<div class="l-footer__container-fixed">
			<div class="l-footer__inner-fixed">
				<div class="l-footer__left">
					<div class="b-two-lines">
						<div class="b-line__light">
							<? echo $properties['TEXT_ABOVE_PHONE']['VALUE'] ?>
						</div>
						<div class="e-line__phone"><? echo $properties['PHONE']['VALUE'] ?></div>
					</div>
				</div>
				<div class="l-footer__right">
					<div class="b-two-lines">
						<div class="e-line__price"><span class="js-total-price"><? echo number_format($_REQUEST['BASE_PRICE'] , 0 , "." , " ") ?></span> <span class="b-price-sign">ь</span></div>
						<div class="b-line__light">
							<? echo $properties['TEXT_ABOVE_PRICE']['VALUE'] ?>
						</div>
					</div>
					<input type="button" class="b-manager-button" value="<? echo $properties['TEXT_BUTTON']['VALUE'] ?>">
				</div>
			</div>
		</div>
	</div>
