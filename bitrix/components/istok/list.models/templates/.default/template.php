<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
$productTypeIBlockId = $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'];
$iBlock = CIBlock::GetById($productTypeIBlockId)->Fetch();
?>
<div class="b-models">
<?
	if (strpos($iBlock['CODE'], '_MODELS_SECTION') === false) {
		$arFilter = array('IBLOCK_ID' => $productTypeIBlockId, 'DEPTH_LEVEL' => 1);
		$rsSect = CIBlockSection::GetList(array('ID' => 'asc'), $arFilter);
		$sections = array();
		$html .= '<div class="b-models">';
		while ($arSect = $rsSect->GetNext()) {
			$arFilter = Array('IBLOCK_ID' => $productTypeIBlockId, 'ID' => $arSect['ID']);
			$db_list = CIBlockSection::GetList(Array(), $arFilter, false, Array("UF_SELECTOR_ICON"))->Fetch();
			$image = CFile::GetPath($db_list['UF_SELECTOR_ICON']);
			$sizes = pathinfo($image);
			$path = ($_SERVER['DOCUMENT_ROOT'] . $image);
			$imageSize = (CFile::GetImageSize($path));
			$width = $imageSize[0];
			$height = $imageSize[1];
			$coefficient = $height / 38;
			if ($coefficient > 1) {
				$newWidth = $width / $coefficient;
				if ($newWidth <= 52) {
					$width = $newWidth;
				} else {
					$width = 52;
				}
			} else {
				$width = 52;
			}
			$html .= <<<EOF
		<div class="b-model" section-id="{$db_list['ID']}">
			<a href="#" class="b-model__link">
				<div style="" class="b-model-icon">
					<div style="
                        background-size: 100%;
                        height: 38px;
                        margin: 0 auto;
                        width: {$width};
                        background-image: url('{$image}');"
						 class="e-model__icon-generic"></div>
				</div>
				<div class="b-model-text">
					{$db_list['NAME']}
				</div>
			</a>
		</div>
EOF;
		}
		$html .= '</div>';
		echo $html;
	} else {
		$selectedModelId = $_REQUEST['MODEL_ID'];
		$list = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']));
		$trailers = array();
		while($element = $list->GetNextElement())
		{
			$blockId = $arParams['IBLOCK_ID'];
			$blockTypeId = $arParams['IBLOCK_ID'];
			$fields = $element->GetFields();
			$elementId = $fields['ID'];
			$elementTypeId = $fields['IBLOCK_ID'];

			if ($blockTypeId != $elementTypeId) {continue;}
			$db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'DETAIL_PICTURE'));
			$picture = $db_props->GetNext();
			$image = CFile::GetPath($fields['DETAIL_PICTURE']);
			$name = $fields['NAME'];
			if ($fields['DETAIL_PICTURE']) {
				$sizes = pathinfo($image);
				$path = ($_SERVER['DOCUMENT_ROOT'].$image);
				$imageSize = (CFile::GetImageSize($path));
				$width = $imageSize[0];
				$height = $imageSize[1];
				$coefficient = $height/38;
				if ($coefficient > 1) {
					$newWidth = $width / $coefficient;
					if ($newWidth <= 52) {
						$width = $newWidth;
					} else {
						$width = 52;
					}
				} else {
					$width = 52;
				}
			}
			?>
			<div class="b-model <? echo $elementId == $selectedModelId ? 'b-model__selected' : '' ?>" item-id="<? echo $elementId ?>">
				<a href="#" class="b-model__link">
					<div style="" class="b-model-icon">
						<div style="
							background-size: 100%;
							height: 38px;
							margin: 0 auto;
							width: <? echo $width ?>;
							background-image: url('<? echo $image ?>');"
							 class="e-model__icon-generic"></div>
					</div>
					<div class="b-model-text">
						<? echo $name ?>
					</div>
				</a>
			</div>
			<?
		}
	}
?>
</div>
