<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<?
	CModule::IncludeModule("iblock");
	$list = CIBlockElement::GetList(
		Array('ID' => 'DESC'),
		Array("IBLOCK_ID"=> $arParams['IBLOCK_ID']),
		false,
		Array ("nTopCount" => 3)
	);
    $news = array();
	while($element = $list->GetNextElement())
	{
		$blockId = $arParams['IBLOCK_ID'];
		$fields = $element->GetFields();
		$elementId = $fields['ID'];
		$elementTypeId = $fields['IBLOCK_ID'];
		if ($blockId != $elementTypeId) {continue;}
		$image = CFile::GetPath($picture = $fields['PREVIEW_PICTURE']);
		$news[$fields['ID']]['TITLE'] = $fields['NAME'];
		$news[$fields['ID']]['TEXT'] = $fields['PREVIEW_TEXT'];
		$news[$fields['ID']]['PICTURE'] = $image;
		$months = array(
			1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
			5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
			9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
		);
		$news[$fields['ID']]['DATE'] = (date('d ') . $months[(date('n'))] . date(' Y'));
	}
?>

<div class="b-articles">
	<div class="b-articles__header">
		<a href="./news" class="e-title__articles-primary">
			новости
		</a>
		<a href="./news" class="e-title__articles-secondary">
			Все новости
		</a>
	</div>
	<?foreach($news as $id => $article){?>
		<div class="b-article">
			<div class="e-article__icon">
				<img src="<? echo $article['PICTURE'] ?>">
			</div>
			<a href="/news/<? echo $id ?>" class="e-article__title">
				<? echo $article['TITLE'] ?>
			</a>

			<div class="e-article__date">
				<? echo $article['DATE'] ?>
			</div>
			<div class="e-article__description">
				<? echo $article['TEXT'] ?>
			</div>
		</div>
	<?}?>
</div>
