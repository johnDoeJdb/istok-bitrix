<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult)) return;
?>

<div class="l-header__center">
    <div class="b-menu">
        <?
            $counter = 0;
            foreach($arResult as $arItem):
        ?>
            <a class="<?= $counter === 0 ? 'b-menu-item__track-icon' : 'b-menu-item' ?>" href="<?=$arItem["LINK"]?>">
                <?=$arItem["TEXT"]?>
            </a>
        <?
            $counter++;
            endforeach;
        ?>
    </div>
</div>
