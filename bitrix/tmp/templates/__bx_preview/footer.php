<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
	<div class="l-footer">
		<div class="l-footer__inner">
			<div class="l-footer__left">
				<?
					$APPLICATION->IncludeComponent(
						"istok:logo",
						".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"IBLOCK_TYPE" => "lists",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "3600",
							"IBLOCK_ID" => "7"
						),
						false
					);
				?>
			</div>
			<div class="l-footer__right">
				<?
				$APPLICATION->IncludeComponent(
					"istok:text.contacts",
					".default",
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_TYPE" => "lists",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "3600",
						"IBLOCK_ID" => "6"
					),
					false
				);
				?>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</body>
</html>