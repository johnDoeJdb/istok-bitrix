window.onload = function() {
    grayscale($('.b-model-icon'));
    $('.b-model-icon').css({
        'filter': 'none',
        'filter': 'none',
        '-webkit-filter': 'initial',
    });
};
$('.b-model').on('mouseover', function() {
    grayscale.reset( $(this).find('.b-model-icon') );
    $(this).find('.b-model-icon').css('opacity', 1);
});
$('.b-model').on('mouseout', function() {
    grayscale( $(this).find('.b-model-icon') );
    $(this).find('.b-model-icon').css('opacity', 0.7);
});
