<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">-->
<head>
    <meta property="og:title" content="Исток">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Istok">
    <meta property="og:locale" content="ru_RU">
    <!--<meta property="og:image" content="./images/favicon/favicon-128.png">-->
    <meta property="og:description" content="Надежные фургоны и приципе для ваших машин">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <? $APPLICATION->ShowHead(); ?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400&subset=latin,cyrillic-ext,cyrillic'
          rel='stylesheet' type='text/css'>
    <link href="//cdn.jsdelivr.net/jquery.gray/1.4.2/gray.min.css" rel="stylesheet">
    <link href="//vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
    <link href="/include/css/jquery.jscrollpane.css" rel="stylesheet">
    <link href="/include/css/normalize.css" rel="stylesheet">
    <link href="/include/css/main.css" rel="stylesheet">
    <script src="/include/js/jquery-2.1.4.min.js"></script>

    <title><? $APPLICATION->ShowTitle() ?></title>
</head>
<body>
<?$APPLICATION->ShowPanel()?>
<div class="l-page">
    <div class="l-header">
        <div class="l-header__left">
            <?
                $APPLICATION->IncludeComponent(
                    "istok:logo",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "IBLOCK_TYPE" => "lists",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "IBLOCK_ID" => "7"
                    ),
                    false
                );
            ?>
        </div>
        <div class="l-header__center">
            <div class="b-menu">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top",
                    array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "COMPONENT_TEMPLATE" => "top",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                ); ?>
            </div>
        </div>
        <div class="l-header__right">
            <?
            $APPLICATION->IncludeComponent(
            	"istok:text.contacts", 
            	".default", 
            	array(
            		"COMPONENT_TEMPLATE" => ".default",
            		"IBLOCK_TYPE" => "lists",
            		"CACHE_TYPE" => "A",
            		"CACHE_TIME" => "3600",
            		"IBLOCK_ID" => "6"
            	),
            	false
            );
            ?>
        </div>
    </div>