<div class="b-models">
<?
    $list = CIBlockElement::GetList(Array());
    while($element = $list->GetNextElement())
    {
		$blockId = $arParams['IBLOCK_ID'];
		$fields = $element->GetFields();
    	$elementId = $fields['ID'];
		$picture = $fields['PICTURE'];
    	$db_props = CIBlockElement::GetProperty($blockId, $elementId, "sort", "asc", Array("CODE"=> 'PICTURE'));
    	$picture = $db_props->GetNext();
        $image = CFile::GetPath($picture["VALUE"]);
    	$name = $fields['NAME'];
?>
        <div class="b-model">
            <a href="#" class="b-model__link">
                <div class="b-model-icon">
                    <div style="background-image: url('<? echo $image ?>')" class="e-model__icon-track"></div>
                </div>
                <div class="b-model-text">
                    <? echo $name ?>
                </div>
            </a>
        </div>
<?
    }
?>
</div>

