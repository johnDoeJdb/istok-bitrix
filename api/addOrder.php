
<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule('sale');

header('Content-Type: application/json');
$postBody = json_decode(file_get_contents('php://input'));

$user = new CUser;
$arFields = Array(
    "EMAIL"             => $postBody->EMAIL,
    "LOGIN"             => "user".time(),
    "PERSONAL_MOBILE"   => $postBody->PHONE,
    "ACTIVE"            => "Y",
    "PASSWORD"          => "123456",
    "CONFIRM_PASSWORD"  => "123456",
);
$userId = $user->Add($arFields);

$arFields = array(
    "LID" => "s1",
    "PERSON_TYPE_ID" => 1,
    "PAYED" => "N",
    "CANCELED" => "N",
    "STATUS_ID" => "N",
    "PRICE" => 0,
    "CURRENCY" => "RUB",
    "USER_ID" => $userId,
    "PAY_SYSTEM_ID" => 1,
    "PRICE_DELIVERY" => 0,
    "DISCOUNT_VALUE" => 0,
    "TAX_VALUE" => 0.0,
    "USER_DESCRIPTION" => ""
);

$ORDER_ID = CSaleOrder::Add($arFields);
$ORDER_ID = IntVal($ORDER_ID);

Add2BasketByProductID($postBody->ELEMENT_ID, 1, array('ORDER_ID' => $ORDER_ID), false);
foreach ($postBody->OPTIONS_IDS as $optionId) {
    Add2BasketByProductID($optionId, 1, array('ORDER_ID' => $ORDER_ID), false);
}
$cntBasketItems = CSaleBasket::GetList(
    array(),
    array(
        "ORDER_ID" => "NULL"
    ),
    false,
    false,
    array()
);
$totalPrice = 0;
while($arBasket = $cntBasketItems->getNext()) {
    $totalPrice += (integer) $arBasket["PRICE"] * (integer) $arBasket["QUANTITY"];
}
$arOrder = CSaleOrder::GetByID($ORDER_ID);
$arFields = array(
    "PRICE" => $totalPrice,
);
CSaleOrder::Update($ORDER_ID, $arFields);
CSaleBasket::OrderBasket($ORDER_ID);

echo json_encode($ORDER_ID);