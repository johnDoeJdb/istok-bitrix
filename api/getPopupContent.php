
<?
header("Cache-Control: max-age=3600");
require_once("./../bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");

header('Content-Type: text/html');
$productTypeIBlockId = $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'];
$sectionId = $_REQUEST['SECTION_ID'];

$arFilter = array('IBLOCK_ID' => $productTypeIBlockId, 'DEPTH_LEVEL' => 1, 'ID' => $sectionId);
$rsSect = CIBlockSection::GetList(array('ID' => 'asc'),$arFilter);
$arSect = $rsSect->Fetch();
?>

<div class="b-popup__black-overlay">
    <div class="e-header__popup"></div>
    <div class="e-title__popup"><? echo $arSect['NAME'] ?></div>
    <div class="b-content__popup">
        <? echo $arSect['DESCRIPTION'] ?>
    </div>
    <input type="button" class="e-button__contact-block-centered" value="Всё понятно">
</div>