<?
header("Cache-Control: max-age=3600");
require_once("./../bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");

$productTypeIBlockId = $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'];
$iBlock = CIBlock::GetById($productTypeIBlockId)->Fetch();
if (strpos($iBlock['CODE'], '_MODELS_SECTION') === false) {
    $arFilter = array('IBLOCK_ID' => $productTypeIBlockId, 'DEPTH_LEVEL' => 1);
    $rsSect = CIBlockSection::GetList(array('ID' => 'asc'),$arFilter);
    $sections = array();
    $html .= '<div class="b-models">';
    while ($arSect = $rsSect->GetNext())
    {
        $arFilter = Array('IBLOCK_ID'=> $productTypeIBlockId,'ID'=>$arSect['ID']);
        $db_list = CIBlockSection::GetList(Array(), $arFilter, false, Array("UF_SELECTOR_ICON"))->Fetch();
        $image = CFile::GetPath($db_list['UF_SELECTOR_ICON']);
        $sizes = pathinfo($image);
        $path = ($_SERVER['DOCUMENT_ROOT'].$image);
        $imageSize = (CFile::GetImageSize($path));
        $width = $imageSize[0];
        $height = $imageSize[1];
        $coefficient = $height/38;
        if ($coefficient > 1) {
            $newWidth = $width / $coefficient;
            if ($newWidth <= 52) {
                $width = $newWidth;
            } else {
                $width = 52;
            }
        } else {
            $width = 52;
        }
        $html .= <<<EOF
        <div class="b-model" section-id="{$db_list['ID']}">
            <a href="#" class="b-model__link">
                <div style="" class="b-model-icon">
                    <div style="
                        background-size: 100%;
                        height: 38px;
                        margin: 0 auto;
                        width: {$width};
                        background-image: url('{$image}');"
                         class="e-model__icon-generic"></div>
                </div>
                <div class="b-model-text">
                    {$db_list['NAME']}
                </div>
            </a>
        </div>
EOF;
    }
    $html .= '</div>';
} else {
    ob_start();
    $APPLICATION->IncludeComponent(
        "istok:list.models",
        ".default",
        Array (
            "COMPONENT_TEMPLATE" => ".default",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "IBLOCK_TYPE" => "lists",
            "IBLOCK_ID" => "4"
        )
    );
    $html = ob_get_contents();
    ob_end_clean();
}
echo $html;
?>







