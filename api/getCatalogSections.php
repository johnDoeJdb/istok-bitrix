
<?
header("Cache-Control: max-age=3600");
require_once("./../bitrix/modules/main/include.php");
CModule::IncludeModule("iblock");
header('Content-Type: application/json');
$productTypeIBlockId = $_REQUEST['PRODUCT_TYPE_IBLOCK_ID'];
//$iBlock = CIBlock::GetById($productTypeIBlockId)->Fetch();

$arFilter = array('IBLOCK_ID' => $productTypeIBlockId, 'DEPTH_LEVEL' => 1);
$arSelect = Array("ID", "POPUP_NAME", "NAME", "DESCRIPTION", "PICTURE", "UF_POPUP_TITLE");
$rsSect = CIBlockSection::GetList(array('ID' => 'asc'), $arFilter, false, $arSelect);
$sections = array();
while ($arSect = $rsSect->GetNext())
{
    $path = CFile::GetPath($arSect['PICTURE']);
    $sections[] = array(
        'ID' => $arSect['ID'],
        'POPUP_NAME' => $arSect['UF_POPUP_TITLE'],
        'NAME' => $arSect['NAME'],
        'DESCRIPTION' => $arSect['DESCRIPTION'],
        'PICTURE' => $path,
    );
}

echo json_encode($sections);